package com.main.nick.chessapp11;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    private Button btn;
    private TextView lastOutcome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn = findViewById(R.id.btnStart);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("My APp","Listener");
                openGameView();
            }
        });

        lastOutcome = findViewById(R.id.lastOutcome);
        lastOutcome.setText("Last Outcome: " + "Unplayed");

    }
    public void openGameView(){
        Intent intent = new Intent(this,gameView.class);
        startActivityForResult(intent,782);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("FINISHED:LSJKF:LSDJKF:KLSD:JKFK:JFJ:KDS:JKFDSJK:LFD:JKLSJKL:");
        switch(requestCode) {
            case (782) : {
                if (resultCode == Activity.RESULT_OK) {
                    // TODO Extract the data returned from the child Activity.
                    String returnValue = data.getStringExtra("some_key");
                    //NORMAL(0), WHITE_WINS_C(1), BLACK_WINS_C(2), STALEMATE(3), DRAW(4), WHITE_WINS_R(5), BLACK_WINS_R(6);

                    switch(returnValue.charAt(0)){
                        case '0':
                            lastOutcome.setText("Last Outcome: " + "Unplayed");
                            break;
                        case '1':
                            lastOutcome.setText("Last Outcome: " + "White Won");
                            break;
                        case '2':
                            lastOutcome.setText("Last Outcome: " + "Black Won");
                            break;
                        case '3':
                            lastOutcome.setText("Last Outcome: " + "StaleMate");
                            break;
                        case '4':
                            lastOutcome.setText("Last Outcome: " + "Draw");
                            break;
                        case '5':
                            lastOutcome.setText("Last Outcome: " + "White Won, Resign");
                            break;
                        case '6':
                            lastOutcome.setText("Last Outcome: " + "Black Won, Resign");
                            break;
                    }

                }
                break;
            }
        }
    }
}
