package com.main.nick.chessapp11;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import game.Game;

import android.widget.Button;
import android.widget.ImageView;
import game.Tile;
import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import android.view.View;
import android.widget.Toast;
import android.graphics.Color;
import 	android.graphics.PorterDuff.Mode;
import android.widget.TextView;




public class gameView extends AppCompatActivity {

    public static ImageView[][] boardPics = new ImageView[8][8];
    private int firstX;
    private int firstY;
    private boolean firstTouch = true;
    private boolean drawone = false;
    private boolean drawCheckConfirm = false;
    Game game = new Game();

    private Button btnResign;
    private Button btnDraw;
    private Button btnAI;
    private Button btnUndo;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_view);
        btnResign = findViewById(R.id.btnResign);
        btnResign.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.nextTurn("resign");

            }
        });
        btnDraw = findViewById(R.id.btnDraw);
        btnDraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!drawCheckConfirm){
                    Toast.makeText(gameView.this, "Initiated Draw", Toast.LENGTH_LONG).show();
                }
                if(drawCheckConfirm){
                    game.nextTurn("draw");
                }
                drawone = true;


            }
        });
        btnAI = findViewById(R.id.btnAI);
        btnAI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        btnUndo = findViewById(R.id.btnUndo);
        btnUndo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });



        boardPics[0][0] = findViewById(R.id.a1);
        boardPics[0][1] = findViewById(R.id.a2);
        boardPics[0][2] = findViewById(R.id.a3);
        boardPics[0][3] = findViewById(R.id.a4);
        boardPics[0][4] = findViewById(R.id.a5);
        boardPics[0][5] = findViewById(R.id.a6);
        boardPics[0][6] = findViewById(R.id.a7);
        boardPics[0][7] = findViewById(R.id.a8);

        boardPics[1][0] = findViewById(R.id.b1);
        boardPics[1][1] = findViewById(R.id.b2);
        boardPics[1][2] = findViewById(R.id.b3);
        boardPics[1][3] = findViewById(R.id.b4);
        boardPics[1][4] = findViewById(R.id.b5);
        boardPics[1][5] = findViewById(R.id.b6);
        boardPics[1][6] = findViewById(R.id.b7);
        boardPics[1][7] = findViewById(R.id.b8);

        boardPics[2][0] = findViewById(R.id.c1);
        boardPics[2][1] = findViewById(R.id.c2);
        boardPics[2][2] = findViewById(R.id.c3);
        boardPics[2][3] = findViewById(R.id.c4);
        boardPics[2][4] = findViewById(R.id.c5);
        boardPics[2][5] = findViewById(R.id.c6);
        boardPics[2][6] = findViewById(R.id.c7);
        boardPics[2][7] = findViewById(R.id.c8);

        boardPics[3][0] = findViewById(R.id.d1);
        boardPics[3][1] = findViewById(R.id.d2);
        boardPics[3][2] = findViewById(R.id.d3);
        boardPics[3][3] = findViewById(R.id.d4);
        boardPics[3][4] = findViewById(R.id.d5);
        boardPics[3][5] = findViewById(R.id.d6);
        boardPics[3][6] = findViewById(R.id.d7);
        boardPics[3][7] = findViewById(R.id.d8);

        boardPics[4][0] = findViewById(R.id.e1);
        boardPics[4][1] = findViewById(R.id.e2);
        boardPics[4][2] = findViewById(R.id.e3);
        boardPics[4][3] = findViewById(R.id.e4);
        boardPics[4][4] = findViewById(R.id.e5);
        boardPics[4][5] = findViewById(R.id.e6);
        boardPics[4][6] = findViewById(R.id.e7);
        boardPics[4][7] = findViewById(R.id.e8);

        boardPics[5][0] = findViewById(R.id.f1);
        boardPics[5][1] = findViewById(R.id.f2);
        boardPics[5][2] = findViewById(R.id.f3);
        boardPics[5][3] = findViewById(R.id.f4);
        boardPics[5][4] = findViewById(R.id.f5);
        boardPics[5][5] = findViewById(R.id.f6);
        boardPics[5][6] = findViewById(R.id.f7);
        boardPics[5][7] = findViewById(R.id.f8);

        boardPics[6][0] = findViewById(R.id.g1);
        boardPics[6][1] = findViewById(R.id.g2);
        boardPics[6][2] = findViewById(R.id.g3);
        boardPics[6][3] = findViewById(R.id.g4);
        boardPics[6][4] = findViewById(R.id.g5);
        boardPics[6][5] = findViewById(R.id.g6);
        boardPics[6][6] = findViewById(R.id.g7);
        boardPics[6][7] = findViewById(R.id.g8);

        boardPics[7][0] = findViewById(R.id.h1);
        boardPics[7][1] = findViewById(R.id.h2);
        boardPics[7][2] = findViewById(R.id.h3);
        boardPics[7][3] = findViewById(R.id.h4);
        boardPics[7][4] = findViewById(R.id.h5);
        boardPics[7][5] = findViewById(R.id.h6);
        boardPics[7][6] = findViewById(R.id.h7);
        boardPics[7][7] = findViewById(R.id.h8);

        for(int i = 0; i<8; i++){
            for(int j = 0; j<8; j++){
                boardPics[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageView img = (ImageView) v;

                        for(int i = 0; i<8; i++) {
                            for (int j = 0; j < 8; j++) {
                                if (boardPics[i][j].getId() == img.getId()) {
                                    img.setColorFilter(Color.YELLOW, Mode.SRC_ATOP);
                                    //Toast.makeText(gameView.this, "You clicked on ImageView", Toast.LENGTH_LONG).show();
                                    if(firstTouch) {
                                        firstX = i;
                                        firstY = j;
                                        firstTouch = false;
                                    }
                                    else{
                                        // a1 a2
                                        firstTouch = true;
                                        char temp;
                                        String moveString = Character.toString(gameView.int2coord(firstX)) + Integer.toString(firstY+1) + " " + Character.toString(gameView.int2coord(i))+ Integer.toString(j+1);

                                        if(drawone){
                                            drawCheckConfirm = true;
                                            moveString = moveString + " draw?";
                                        }else{
                                            drawCheckConfirm = false;
                                        }
                                        //Toast.makeText(gameView.this, moveString, Toast.LENGTH_LONG).show();
                                        game.nextTurn(moveString);
                                        drawone = false;
                                        break;
                                    }
                                }
                                else {
                                    boardPics[i][j].setColorFilter(null);
                                }
                            }
                        }
                        if(firstTouch){
                            for(int i = 0; i<8; i++) {
                                for (int j = 0; j < 8; j++) {
                                    boardPics[i][j].setColorFilter(null);
                                }
                            }
                        }
                    }

                });
            }
        }
        game.playGame(boardPics, this);

    }
    public static void updateView(Tile[][] chessBoard, Piece[] piecesArray){
        int rankNumber = 8;
        for(int i=chessBoard.length; i>0; i--) {
            for(int j=1; j<=chessBoard[0].length; j++) {
                String tileString = "";
                //If tile is not occupied, print "##" for black squares, print "  " for white squares
                //If tile is occupied, check if white or black first, then check piece type
                if(!chessBoard[i-1][j-1].isOccupied()) {
                    if((i+j)%2 == 1) {
                        //Odd means white square
                        boardPics[j-1][i-1].setImageResource(R.drawable.w);
                    }else {
                        //Even means black square
                        boardPics[j-1][i-1].setImageResource(R.drawable.b);
                    }
                }else {
                    Piece piece = getPieceFromCoordinate(j, i, piecesArray);
                    int pieceColor = piece.getColor();
                    if(pieceColor == 0) {
                        if(piece instanceof Pawn) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_pawn);
                        }else if(piece instanceof Rook) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_rook);
                        }else if(piece instanceof Knight) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_knight);
                        }else if(piece instanceof Bishop) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_bishop);
                        }else if(piece instanceof Queen) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_queen);
                        }else if(piece instanceof King) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.w_king);
                        }
                    }else {
                        if(piece instanceof Pawn) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_pawn);
                        }else if(piece instanceof Rook) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_rook);
                        }else if(piece instanceof Knight) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_knight);
                        }else if(piece instanceof Bishop) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_bishop);
                        }else if(piece instanceof Queen) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_queen);
                        }else if(piece instanceof King) {
                            boardPics[j-1][i-1].setImageResource(R.drawable.b_king);
                        }
                    }
                }
            }
            rankNumber--;
        }
    }
    public static Piece getPieceFromCoordinate(int x_coordinate, int y_coordinate, Piece[] piecesArray) {
        for(int i=0; i<piecesArray.length; i++) {
            if(piecesArray[i] == null) {
                continue;
            }else if(piecesArray[i].getX_coordinate() == x_coordinate && piecesArray[i].getY_coordinate() == y_coordinate) {
                return piecesArray[i];
            }
        }
        return null;
    }

    public static char int2coord(int x){
        switch(x) {
            case 0:
                return 'a';
            case 1:
                return 'b';
            case 2:
                return 'c';
            case 3:
                return 'd';
            case 4:
                return 'e';
            case 5:
                return 'f';
            case 6:
                return 'g';
            case 7:
                return 'h';
        }
        return ' ';
    }
    public void end(int state){
        //TextView tv = findViewById(R.id.lastOutcome);
        //tv.setText("Last Outcome: " + MainActivity.gs);
        Intent resultIntent = new Intent();

        resultIntent.putExtra("some_key",Integer.toString(state));
        setResult(Activity.RESULT_OK, resultIntent);
        finish();
    }


}
