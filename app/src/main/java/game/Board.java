package game;

import java.util.ArrayList;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Move;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;
import android.widget.ImageView;
import com.main.nick.chessapp11.gameView;

import com.main.nick.chessapp11.R;

/** Stores all pieces and tiles
 * 	Used to set all pieces in their starting position
 * 	Used to check if king is in check
 *  
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public class Board {
	
	//Chess Board
	//public static Tile[][] chessBoard = new Tile[8][8];
	private Tile[][] chessBoard;
	private Piece[] piecesArray;
	private ImageView[][] board_pieces;

	public ImageView[][] boardPics = new ImageView[8][8];

	//Array containing all pieces and relevant information such as location, color, id, etc.
	//public static Piece[] piecesArray = new Piece[32];

	public ImageView[][] getPicsBoard() {
		return board_pieces;
	}

	public void setPicsBoard(ImageView[][] board_pieces) {
		this.board_pieces = board_pieces;
	}

	public Tile[][] getChessBoard() {
		return chessBoard;
	}

	public void setChessBoard(Tile[][] chessBoard) {
		this.chessBoard = chessBoard;
	}

	public Piece[] getPiecesArray() {
		return piecesArray;
	}

	public void setPiecesArray(Piece[] piecesArray) {
		this.piecesArray = piecesArray;
	}
	/** Starts a game and puts all pieces in their place
	 * 
	 * @param chessBoard The current board
	 * @param piecesArray array of all pieces and their location
	 */
	public void initializeGame(Tile[][] chessBoard, Piece[] piecesArray) {
		//First set everything on the chessBoard to unoccupied
		//Then change each tile status as each piece is created and placed on the board
		for(int i=chessBoard.length; i>0; i--) {
			for(int j=1; j<=chessBoard[0].length; j++) {
				chessBoard[i-1][j-1].setOccupied(false);
				chessBoard[i-1][j-1].setX_coordinate(j);
				chessBoard[i-1][j-1].setY_coordinate(i);
			}
		}
		
		//Create the white pawns and initialize their positions
		Piece white_pawn1 = new Pawn();
		white_pawn1.setColor(0);
		white_pawn1.setPiece_id(0);
		white_pawn1.setX_coordinate(1);
		white_pawn1.setY_coordinate(2);
		chessBoard[white_pawn1.getY_coordinate()-1][white_pawn1.getX_coordinate()-1].setOccupied(true);
		piecesArray[0] = white_pawn1;
		
		Piece white_pawn2 = new Pawn();
		white_pawn2.setColor(0);
		white_pawn2.setPiece_id(1);
		white_pawn2.setX_coordinate(2);
		white_pawn2.setY_coordinate(2);
		chessBoard[white_pawn2.getY_coordinate()-1][white_pawn2.getX_coordinate()-1].setOccupied(true);
		piecesArray[1] = white_pawn2;
		
		Piece white_pawn3 = new Pawn();
		white_pawn3.setColor(0);
		white_pawn3.setPiece_id(2);
		white_pawn3.setX_coordinate(3);
		white_pawn3.setY_coordinate(2);
		chessBoard[white_pawn3.getY_coordinate()-1][white_pawn3.getX_coordinate()-1].setOccupied(true);
		piecesArray[2] = white_pawn3;
		
		Piece white_pawn4 = new Pawn();
		white_pawn4.setColor(0);
		white_pawn4.setPiece_id(3);
		white_pawn4.setX_coordinate(4);
		white_pawn4.setY_coordinate(2);
		chessBoard[white_pawn4.getY_coordinate()-1][white_pawn4.getX_coordinate()-1].setOccupied(true);
		piecesArray[3] = white_pawn4;
		
		Piece white_pawn5 = new Pawn();
		white_pawn5.setColor(0);
		white_pawn5.setPiece_id(4);
		white_pawn5.setX_coordinate(5);
		white_pawn5.setY_coordinate(2);
		chessBoard[white_pawn5.getY_coordinate()-1][white_pawn5.getX_coordinate()-1].setOccupied(true);
		piecesArray[4] = white_pawn5;
		
		Piece white_pawn6 = new Pawn();
		white_pawn6.setColor(0);
		white_pawn6.setPiece_id(5);
		white_pawn6.setX_coordinate(6);
		white_pawn6.setY_coordinate(2);
		chessBoard[white_pawn6.getY_coordinate()-1][white_pawn6.getX_coordinate()-1].setOccupied(true);
		piecesArray[5] = white_pawn6;
		
		Piece white_pawn7 = new Pawn();
		white_pawn7.setColor(0);
		white_pawn7.setPiece_id(6);
		white_pawn7.setX_coordinate(7);
		white_pawn7.setY_coordinate(2);
		chessBoard[white_pawn7.getY_coordinate()-1][white_pawn7.getX_coordinate()-1].setOccupied(true);
		piecesArray[6] = white_pawn7;
		
		Piece white_pawn8 = new Pawn();
		white_pawn8.setColor(0);
		white_pawn8.setPiece_id(7);
		white_pawn8.setX_coordinate(8);
		white_pawn8.setY_coordinate(2);
		chessBoard[white_pawn8.getY_coordinate()-1][white_pawn8.getX_coordinate()-1].setOccupied(true);
		piecesArray[7] = white_pawn8;
		
		//Create the white rooks and initialize their positions
		Piece white_rook1 = new Rook();
		white_rook1.setColor(0);
		white_rook1.setPiece_id(8);
		white_rook1.setX_coordinate(1);
		white_rook1.setY_coordinate(1);
		chessBoard[white_rook1.getY_coordinate()-1][white_rook1.getX_coordinate()-1].setOccupied(true);
		piecesArray[8] = white_rook1;
		
		Piece white_rook2 = new Rook();
		white_rook2.setColor(0);
		white_rook2.setPiece_id(9);
		white_rook2.setX_coordinate(8);
		white_rook2.setY_coordinate(1);
		chessBoard[white_rook2.getY_coordinate()-1][white_rook2.getX_coordinate()-1].setOccupied(true);
		piecesArray[9] = white_rook2;
		
		//Create the white knights and initialize their positions
		Piece white_knight1 = new Knight();
		white_knight1.setColor(0);
		white_knight1.setPiece_id(10);
		white_knight1.setX_coordinate(2);
		white_knight1.setY_coordinate(1);
		chessBoard[white_knight1.getY_coordinate()-1][white_knight1.getX_coordinate()-1].setOccupied(true);
		piecesArray[10] = white_knight1;
		
		Piece white_knight2 = new Knight();
		white_knight2.setColor(0);
		white_knight2.setPiece_id(11);
		white_knight2.setX_coordinate(7);
		white_knight2.setY_coordinate(1);
		chessBoard[white_knight2.getY_coordinate()-1][white_knight2.getX_coordinate()-1].setOccupied(true);
		piecesArray[11] = white_knight2;
		
		//Create the white bishops and initialize their positions
		Piece white_bishop1 = new Bishop();
		white_bishop1.setColor(0);
		white_bishop1.setPiece_id(12);
		white_bishop1.setX_coordinate(3);
		white_bishop1.setY_coordinate(1);
		chessBoard[white_bishop1.getY_coordinate()-1][white_bishop1.getX_coordinate()-1].setOccupied(true);
		piecesArray[12] = white_bishop1;
		
		Piece white_bishop2 = new Bishop();
		white_bishop2.setColor(0);
		white_bishop2.setPiece_id(13);
		white_bishop2.setX_coordinate(6);
		white_bishop2.setY_coordinate(1);
		chessBoard[white_bishop2.getY_coordinate()-1][white_bishop2.getX_coordinate()-1].setOccupied(true);
		piecesArray[13] = white_bishop2;
		
		//Create the white queen and initialize its position
		Piece white_queen = new Queen();
		white_queen.setColor(0);
		white_queen.setPiece_id(14);
		white_queen.setX_coordinate(4);
		white_queen.setY_coordinate(1);
		chessBoard[white_queen.getY_coordinate()-1][white_queen.getX_coordinate()-1].setOccupied(true);
		piecesArray[14] = white_queen;
		
		//Create the white king and initialize its position
		Piece white_king = new King();
		white_king.setColor(0);
		white_king.setPiece_id(15);
		white_king.setX_coordinate(5);
		white_king.setY_coordinate(1);
		chessBoard[white_king.getY_coordinate()-1][white_king.getX_coordinate()-1].setOccupied(true);
		piecesArray[15] = white_king;
		
		//Create the black pawns and initialize their positions
		Piece black_pawn1 = new Pawn();
		black_pawn1.setColor(1);
		black_pawn1.setPiece_id(16);
		black_pawn1.setX_coordinate(1);
		black_pawn1.setY_coordinate(7);
		chessBoard[black_pawn1.getY_coordinate()-1][black_pawn1.getX_coordinate()-1].setOccupied(true);
		piecesArray[16] = black_pawn1;
		
		Piece black_pawn2 = new Pawn();
		black_pawn2.setColor(1);
		black_pawn2.setPiece_id(17);
		black_pawn2.setX_coordinate(2);
		black_pawn2.setY_coordinate(7);
		chessBoard[black_pawn2.getY_coordinate()-1][black_pawn2.getX_coordinate()-1].setOccupied(true);
		piecesArray[17] = black_pawn2;
		
		Piece black_pawn3 = new Pawn();
		black_pawn3.setColor(1);
		black_pawn3.setPiece_id(18);
		black_pawn3.setX_coordinate(3);
		black_pawn3.setY_coordinate(7);
		chessBoard[black_pawn3.getY_coordinate()-1][black_pawn3.getX_coordinate()-1].setOccupied(true);
		piecesArray[18] = black_pawn3;
		
		Piece black_pawn4 = new Pawn();
		black_pawn4.setColor(1);
		black_pawn4.setPiece_id(19);
		black_pawn4.setX_coordinate(4);
		black_pawn4.setY_coordinate(7);
		chessBoard[black_pawn4.getY_coordinate()-1][black_pawn4.getX_coordinate()-1].setOccupied(true);
		piecesArray[19] = black_pawn4;
		
		Piece black_pawn5 = new Pawn();
		black_pawn5.setColor(1);
		black_pawn5.setPiece_id(20);
		black_pawn5.setX_coordinate(5);
		black_pawn5.setY_coordinate(7);
		chessBoard[black_pawn5.getY_coordinate()-1][black_pawn5.getX_coordinate()-1].setOccupied(true);
		piecesArray[20] = black_pawn5;
		
		Piece black_pawn6 = new Pawn();
		black_pawn6.setColor(1);
		black_pawn6.setPiece_id(21);
		black_pawn6.setX_coordinate(6);
		black_pawn6.setY_coordinate(7);
		chessBoard[black_pawn6.getY_coordinate()-1][black_pawn6.getX_coordinate()-1].setOccupied(true);
		piecesArray[21] = black_pawn6;
		
		Piece black_pawn7 = new Pawn();
		black_pawn7.setColor(1);
		black_pawn7.setPiece_id(22);
		black_pawn7.setX_coordinate(7);
		black_pawn7.setY_coordinate(7);
		chessBoard[black_pawn7.getY_coordinate()-1][black_pawn7.getX_coordinate()-1].setOccupied(true);
		piecesArray[22] = black_pawn7;
		
		Piece black_pawn8 = new Pawn();
		black_pawn8.setColor(1);
		black_pawn8.setPiece_id(23);
		black_pawn8.setX_coordinate(8);
		black_pawn8.setY_coordinate(7);
		chessBoard[black_pawn8.getY_coordinate()-1][black_pawn8.getX_coordinate()-1].setOccupied(true);
		piecesArray[23] = black_pawn8;
		
		//Create the black rooks and initialize their positions
		Piece black_rook1 = new Rook();
		black_rook1.setColor(1);
		black_rook1.setPiece_id(24);
		black_rook1.setX_coordinate(1);
		black_rook1.setY_coordinate(8);
		chessBoard[black_rook1.getY_coordinate()-1][black_rook1.getX_coordinate()-1].setOccupied(true);
		piecesArray[24] = black_rook1;
		
		Piece black_rook2 = new Rook();
		black_rook2.setColor(1);
		black_rook2.setPiece_id(25);
		black_rook2.setX_coordinate(8);
		black_rook2.setY_coordinate(8);
		chessBoard[black_rook2.getY_coordinate()-1][black_rook2.getX_coordinate()-1].setOccupied(true);
		piecesArray[25] = black_rook2;
		
		//Create the black knights and initialize their positions
		Piece black_knight1 = new Knight();
		black_knight1.setColor(1);
		black_knight1.setPiece_id(26);
		black_knight1.setX_coordinate(2);
		black_knight1.setY_coordinate(8);
		chessBoard[black_knight1.getY_coordinate()-1][black_knight1.getX_coordinate()-1].setOccupied(true);
		piecesArray[26] = black_knight1;
		
		Piece black_knight2 = new Knight();
		black_knight2.setColor(1);
		black_knight2.setPiece_id(27);
		black_knight2.setX_coordinate(7);
		black_knight2.setY_coordinate(8);
		chessBoard[black_knight2.getY_coordinate()-1][black_knight2.getX_coordinate()-1].setOccupied(true);
		piecesArray[27] = black_knight2;
		
		//Create the black bishops and initialize their positions
		Piece black_bishop1 = new Bishop();
		black_bishop1.setColor(1);
		black_bishop1.setPiece_id(28);
		black_bishop1.setX_coordinate(3);
		black_bishop1.setY_coordinate(8);
		chessBoard[black_bishop1.getY_coordinate()-1][black_bishop1.getX_coordinate()-1].setOccupied(true);
		piecesArray[28] = black_bishop1;
		
		Piece black_bishop2 = new Bishop();
		black_bishop2.setColor(1);
		black_bishop2.setPiece_id(29);
		black_bishop2.setX_coordinate(6);
		black_bishop2.setY_coordinate(8);
		chessBoard[black_bishop2.getY_coordinate()-1][black_bishop2.getX_coordinate()-1].setOccupied(true);
		piecesArray[29] = black_bishop2;
		
		//Create the black queen and initialize its position
		Piece black_queen = new Queen();
		black_queen.setColor(1);
		black_queen.setPiece_id(30);
		black_queen.setX_coordinate(4);
		black_queen.setY_coordinate(8);
		chessBoard[black_queen.getY_coordinate()-1][black_queen.getX_coordinate()-1].setOccupied(true);
		piecesArray[30] = black_queen;
		
		//Create the black king and initialize its position
		Piece black_king = new King();
		black_king.setColor(1);
		black_king.setPiece_id(31);
		black_king.setX_coordinate(5);
		black_king.setY_coordinate(8);
		chessBoard[black_king.getY_coordinate()-1][black_king.getX_coordinate()-1].setOccupied(true);
		piecesArray[31] = black_king;
	}
	/** Prints current board and pieces
	 * 
	 * @param chessBoard The current board
	 * @param piecesArray array of all pieces and their location
	 */
	public void printBoard(Tile[][] chessBoard, Piece[] piecesArray) {
		int rankNumber = 8;
		for(int i=chessBoard.length; i>0; i--) {
			for(int j=1; j<=chessBoard[0].length; j++) {
				String tileString = "";
				//If tile is not occupied, print "##" for black squares, print "  " for white squares
				//If tile is occupied, check if white or black first, then check piece type
				if(!chessBoard[i-1][j-1].isOccupied()) {
					if((i+j)%2 == 1) {
						//Odd means white square
						tileString = "   ";
					}else {
						//Even means black square
						tileString = "## ";
					}
				}else {
					Piece piece = getPieceFromCoordinate(j, i, piecesArray);
					int pieceColor = piece.getColor();
					if(pieceColor == 0) {
						tileString += "w";
						if(piece instanceof Pawn) {
							tileString += "p ";
							//board_pieces[i][j].setImageResource(R.drawable.w_pawn);
						}else if(piece instanceof Rook) {
							tileString += "R ";
							//board_pieces[i][j].setImageResource(R.drawable.w_rook);
						}else if(piece instanceof Knight) {
							tileString += "N ";
							//board_pieces[i][j].setImageResource(R.drawable.w_knight);
						}else if(piece instanceof Bishop) {
							tileString += "B ";
							//board_pieces[i][j].setImageResource(R.drawable.w_bishop);
						}else if(piece instanceof Queen) {
							tileString += "Q ";
							//board_pieces[i][j].setImageResource(R.drawable.w_queen);
						}else if(piece instanceof King) {
							tileString += "K ";
							//board_pieces[i][j].setImageResource(R.drawable.w_king);
						}
					}else {
						tileString += "b";
						if(piece instanceof Pawn) {
							tileString += "p ";
							//board_pieces[i][j].setImageResource(R.drawable.b_pawn);
						}else if(piece instanceof Rook) {
							tileString += "R ";
							//board_pieces[i][j].setImageResource(R.drawable.b_rook);
						}else if(piece instanceof Knight) {
							tileString += "N ";
							//board_pieces[i][j].setImageResource(R.drawable.b_knight);
						}else if(piece instanceof Bishop) {
							tileString += "B ";
							//board_pieces[i][j].setImageResource(R.drawable.b_bishop);
						}else if(piece instanceof Queen) {
							tileString += "Q ";
							//board_pieces[i][j].setImageResource(R.drawable.b_queen);
						}else if(piece instanceof King) {
							tileString += "K ";
							//board_pieces[i][j].setImageResource(R.drawable.b_king);
						}
					}
					

				}
				System.out.print(tileString);
			}
			System.out.print(rankNumber);
			rankNumber--;
			System.out.println();
		}
		System.out.println(" a  b  c  d  e  f  g  h");
		gameView.updateView(chessBoard,piecesArray);
	}
	/** Returns piece at specified coordinate
	 * 
	 * @param x_coordinate X coordinate to look at
	 * @param y_coordinate Y coordinte to look at
	 * @param piecesArray array of all pieces and their location
	 * @return Piece at location, or null if none exist
	 */
	public Piece getPieceFromCoordinate(int x_coordinate, int y_coordinate, Piece[] piecesArray) {
		for(int i=0; i<piecesArray.length; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else if(piecesArray[i].getX_coordinate() == x_coordinate && piecesArray[i].getY_coordinate() == y_coordinate) {
				return piecesArray[i];
			}
		}
		return null;
	}
	
/** Checks if a particular move will result in check
 * 
 * @param originalChessBoard The current board
 * @param piecesArray array of all pieces and their location
 * @param potentialMove Move that is about to be made
 * @param gameLog List of all previous moves
 * @return True if in check, False Otherwise
 */
	public int resultInCheck(Tile[][] originalChessBoard, Piece[] piecesArray, Move potentialMove, ArrayList<Turn> gameLog) {
		//Tile[][] tempChessBoard = originalChessBoard.clone();
		//Piece[] tempPiecesArray = piecesArray.clone();
		
		Tile[][] tempChessBoard = new Tile[8][8];
		for(int i=0; i<8; i++) {
			for(int j=0; j<8; j++) {
				tempChessBoard[i][j] = new Tile();
				tempChessBoard[i][j].setOccupied(originalChessBoard[i][j].isOccupied());
				tempChessBoard[i][j].setX_coordinate(originalChessBoard[i][j].getX_coordinate());
				tempChessBoard[i][j].setY_coordinate(originalChessBoard[i][j].getY_coordinate());
			}
		}
		
		Piece[] tempPiecesArray = new Piece[32];
		for(int i=0; i<32; i++) {
			if(piecesArray[i] == null) {
				tempPiecesArray[i] = null;
			}else {
				if((i>=0 && i<=7) || (i>=16 && i<=23)) {
					//These indices indicate that piece is pawn
					Piece tempPiece = new Pawn();
					tempPiecesArray[i] = tempPiece;
				}else if(i==8 || i==9 || i==24 || i== 25) {
					//These indices indicate that piece is rook
					Piece tempPiece = new Rook();
					tempPiecesArray[i] = tempPiece;
				}else if(i==10 || i==11 || i==26 || i==27) {
					//These indices indicate that piece is knight
					Piece tempPiece = new Knight();
					tempPiecesArray[i] = tempPiece;
				}else if(i==12 || i==13 || i==28 || i==29) {
					//These indices indicate that piece is bishop
					Piece tempPiece = new Bishop();
					tempPiecesArray[i] = tempPiece;
				}else if(i==14 || i==30) {
					//These indices indicate that piece is queen
					Piece tempPiece = new Queen();
					tempPiecesArray[i] = tempPiece;
				}else if(i==15 || i==31) {
					//These indices indicate that piece is king
					Piece tempPiece = new King();
					tempPiecesArray[i] = tempPiece;
				}
				tempPiecesArray[i].setColor(piecesArray[i].getColor());
				tempPiecesArray[i].setPiece_id(piecesArray[i].getPiece_id());
				tempPiecesArray[i].setX_coordinate(piecesArray[i].getX_coordinate());
				tempPiecesArray[i].setY_coordinate(piecesArray[i].getY_coordinate());
			}
		}
		
		int current_x = potentialMove.getCurrent_x();
		int current_y = potentialMove.getCurrent_y();
		
		int check = 0;
		
		for(int i=0; i<tempPiecesArray.length; i++) {
			if(tempPiecesArray[i] == null) {
				continue;
			}else if(tempPiecesArray[i].getX_coordinate() == current_x && tempPiecesArray[i].getY_coordinate() == current_y) {
				if(potentialMove.getMove_type() == 0) {
					//Regular Move
					tempPiecesArray[i].move(tempChessBoard, tempPiecesArray, potentialMove);
					check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
				}else if(potentialMove.getMove_type() == 1) {
					//Capture Move
					tempPiecesArray[i].capture(tempChessBoard, tempPiecesArray, potentialMove);
					check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
				}else if(potentialMove.getMove_type() == 2) {
					//Castle short for king, Promotion for pawn
					if(tempPiecesArray[i] instanceof King) {
						King king = (King) tempPiecesArray[i];
						king.castle_short(tempChessBoard, tempPiecesArray, potentialMove);
						check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
					}else {
						Pawn pawn = (Pawn) tempPiecesArray[i];
						pawn.move(tempChessBoard, tempPiecesArray, potentialMove);
						check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
					}
				}else if(potentialMove.getMove_type() == 3) {
					//Castle long for king, En passant for pawn
					if(tempPiecesArray[i] instanceof King) {
						King king = (King) tempPiecesArray[i];
						king.castle_long(tempChessBoard, tempPiecesArray, potentialMove);
						check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
					}else {
						Pawn pawn = (Pawn) tempPiecesArray[i];
						pawn.en_passant(tempChessBoard, tempPiecesArray, potentialMove);
						check = inCheck(tempChessBoard, tempPiecesArray, gameLog);
					}
				}
			}
		}
		return check;
	}
	
	/** Checks if any of the kings are in check
	 * 
	 * @param chessBoard The current board
	 * @param piecesArray array of all pieces and their location
	 * @param gameLog List of all previous moves
	 * @return 0 if nobody is in check, 1 if white is in check, 2 if black is in check
	 */
	public int inCheck(Tile[][] chessBoard, Piece[] piecesArray, ArrayList<Turn> gameLog) {
		//Returns 0 if nobody is in check, 1 if white is in check, 2 if black is in check
		int check = 0;
		//Piece ID's of the white and black king are 15 and 31 respectively
		//Piece ID's correspond to the index in the piecesArray
		//Get current location of white king
		int white_king_x = piecesArray[15].getX_coordinate();
		int white_king_y = piecesArray[15].getY_coordinate();
		//Loops through black pieces to see if each piece's move set contains a move that threatens white king
		for(int i=16; i<32; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else {
				//Threw in this if statement block to prevent program from recursing infinitely
				if(i==31) {
					King black_king = (King) piecesArray[i];
					if(black_king.isMoved() == false) {
						continue;
					}
				}
				ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(chessBoard, this, piecesArray, gameLog);
				for(int j=0; j<movesList.size(); j++) {
					int x = movesList.get(j).getNew_x();
					int y = movesList.get(j).getNew_y();
					
					if(x == white_king_x && y == white_king_y) {
						check = 1;
						break;
					}
				}
			}
			if(check == 1) break;
		}
		//Get current location of black king
		int black_king_x = piecesArray[31].getX_coordinate();
		int black_king_y = piecesArray[31].getY_coordinate();
		//Loops through white pieces and gets moves that threaten king
		for(int i=0; i<16; i++) {
			if(piecesArray[i] == null) {
				continue;
			}else {
				//Threw in this if statement block to prevent program from recursing infinitely
				if(i==15) {
					King white_king = (King) piecesArray[i];
					if(white_king.isMoved() == false) {
						continue;
					}
				}
				ArrayList<Move> movesList = piecesArray[i].getPossibleMoves(chessBoard, this, piecesArray, gameLog);
				for(int j=0; j<movesList.size(); j++) {
					int x = movesList.get(j).getNew_x();
					int y = movesList.get(j).getNew_y();
					
					if(x == black_king_x && y == black_king_y) {
						check = 2;
						break;
					}
				}
			}
			if(check == 2) break;
		}
		
		//System.out.println(check);
		return check;
	}

}
