package game;

import java.util.ArrayList;
import java.util.Random;

import pieces.Move;

public class Kasparov {

    private Kasparov(){};

    public Move choose_ai_Move(ArrayList<Move> validMoves){
        Move move = null;

        int arraySize = validMoves.size();

        if(arraySize > 0){
            Random random = new Random();
            int randomIndex = random.nextInt(arraySize);
            move = validMoves.get(randomIndex);
        }

        return move;
    }



}
