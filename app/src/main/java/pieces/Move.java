package pieces;

import java.io.Serializable;

/** Used to store information for a move event
 * 
 * @author Nick Clegg
 * @author Kevin Wu
 * @version 1.0
 *
 */
public class Move implements Serializable{
	
	int current_x;
	int current_y;
	int new_x;
	int new_y;
	
	/** 0 for regular move, 1 for capture, 2 for special_1, 3 for special_2
	 */
	int move_type;
	
	/** Data needed for a move event
	 * <p>
	 * Special_1 or move_type = 2 is defined as promotion
	 * Special_2 or move_type = 3 is defined as en passant
	 * @param current_x Current x position
	 * @param current_y Current y position
	 * @param new_x New x position
	 * @param new_y New y position
	 * @param move_type Move type 0 for regular move, 1 for capture, 2 for special_1, 3 for special_2
	 */
	public Move(int current_x, int current_y, int new_x, int new_y, int move_type) {
		this.current_x = current_x;
		this.current_y = current_y;
		this.new_x = new_x;
		this.new_y = new_y;
		this.move_type = move_type;
	}
	
	public int getNew_x() {
		return new_x;
	}

	public void setNew_x(int new_x) {
		this.new_x = new_x;
	}
	
	public int getNew_y() {
		return new_y;
	}

	public void setNew_y(int new_y) {
		this.new_y = new_y;
	}

	public int getCurrent_x() {
		return current_x;
	}

	public void setCurrent_x(int current_x) {
		this.current_x = current_x;
	}

	public int getCurrent_y() {
		return current_y;
	}

	public void setCurrent_y(int current_y) {
		this.current_y = current_y;
	}

	public int getMove_type() {
		return move_type;
	}

	public void setMove_type(int move_type) {
		this.move_type = move_type;
	}

	public String toString() {
		return Integer.toString(this.current_x) + Integer.toString(this.current_y) + " " + Integer.toString(this.new_x) + Integer.toString(this.new_y) + " : " + Integer.toString(this.move_type);
	}
}
